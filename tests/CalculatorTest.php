<?php
namespace Lcube\PhpUnit\Tests;

use Lcube\PhpUnit\Calculator;
use PHPUnit_Framework_TestCase;

class CalculatorTest extends PHPUnit_Framework_TestCase
{
    /* @var Calculator */
    private $calculator;

    /**
     * setUp
     *
     * @test
     * @return void
     */
    public function setUp()
    {
        $this->calculator = new Calculator();
    }

    /**
     * itAddsTwoNumbers test
     *
     * @test
     * @return void
     */
    public function itAddsTwoNumbers()
    {
        $expected = 2;
        $actual = $this->calculator->add(1, 1);
        $this->assertEquals($expected, $actual);
    }

    /**
     * itAddsTwoNumbers test
     *
     * @test
     * @return void
     */
    public function itAddsTwoNumbersAndCrash()
    {
        $expected = 20;
        $actual = $this->calculator->add(10, 10);
        $this->assertEquals($expected, $actual);
    }
}